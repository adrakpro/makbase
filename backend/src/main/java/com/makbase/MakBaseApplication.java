package com.makbase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MakBaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(MakBaseApplication.class, args);
    }

}
